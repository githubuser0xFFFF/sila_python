.. sila_python documentation master file, created by
   sphinx-quickstart on Sat Aug 17 19:51:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

|sila-python| SiLA-pythons General Documentation
========================================================

In this documentation you will find more general information about **sila_python**.
For details about our packages, like `sila2lib <https://gitlab.com/sila2/sila_python/tree/master/sila_library>`_,
`sila2codegenerator <https://gitlab.com/sila2/sila_python/tree/master/sila_tools/sila2codegenerator>`_,
please refer to the packages documentation.

Project Version
----------------
.. include:: ../VERSION

Installation
-------------

Before you can run a SiLA 2 client and/or server, some packages need to be installed.

For a quick installation, we provided an installer script *sila2install.py*.

Please make sure that you have python > 3.6 installed on your system, e.g. by typing

.. code:: console

    python3 --version

If python3 in version *>3.6* is installed on your system, you can type on any terminal

.. code-block:: console

     cd [directory of sila_python]
     python3 sila2install.py

and should be ready to go.

For more specific installations,e.g. on a certain opertion system, please refer to: :doc:`installation/0_installation`

Quickstart
-----------

Get SiLA2 running in less than 5-10 minutes (depending on your internet connection) with our
  Quickstart Tutorial :doc:`tutorials/1_quickstart`

For ultra-fast code generation, please look into our sila2codegenerator quickstart tutorial:
  `sila2codegenerator tutorial <https://gitlab.com/sila2/sila_python/tree/master/sila_tools/sila2codegenerator>`_

For fast installation of SiLA 2 Server on a Raspberry Pi 3/B+ :
  `Raspberry Pi Installation <https://gitlab.com/sila2/sila_python/tree/master/raspberry_pi>`_

Developing for sila_python
---------------------------

We arve very glad, that you are considering developing for *SiLA_python*:
Please find a comprehensive guide for you: :doc:`development/1_contributing_sila_python`.

Table of Contents
--------------------

.. toctree::
  :glob:
  :maxdepth: 2
  :titlesonly:

  intro
  installation/*

  development/*

  build_package
  modules
  todo
  changelog


.. automodule:: models

.. autoclass::
     :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. |sila-python| image:: ./images/sila-python-logo.png
