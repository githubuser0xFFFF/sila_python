Quickstart
==========

Installation
_____________

Please make sure, that pip3 and python3-venv is installed
(it is highly recommended to install all python3 components in a `virtual environment <> `_  ).

.. code-block:: console

  ( sudo apt install python3-pip # this you might need to run on Debian/Ubuntu/Mint Linux   )
  pip3 install python3-venv      # on MSWindows, pip3 comes with with the latest python3 installation

Clone the git repository
-------------------------

.. code-block:: console

  git clone https://gitlab.com/SiLA2/sila_python

  cd sila_python

  python3 sila2installer.py  # this installs a python virtualenv and all dependencies

For other installtion options, please refer to SiLA 2 :doc:`installation/0_installation`


Checking the Installation
--------------------------

.. code-block:: console

   python3
   >>>import sila2lib  # no error should occure executing this line


Running the first SiLA2 server / client
________________________________________

Open two terminals and activate the generated sila2 virtual environment with

.. code-block:: console

   source [path to virtualenv]/bin/activate  # on Linux

   # or on MSWindows in cmd.exe

   [path to venv]/scripts/activate.bat  # for MSPowershell, use acitvate.ps2

an activated virtual environment is indicated by '(name of venv)' in the command prompt.

.. code-block:: console

   cd examples/HelloSiLA2

   # in the first terminal, please enter

   python3 HelloSiLA2_server.py  # see README.rst there for correct output

   # in the second terminal on the same machine, enter

   python3 HelloSiLA2_client.py  # see README.rst there for correct output

**Congratulations !** You now managed to run your fist SiLA 2 server client pair :)

What have been achieved by now ?

  * starting the SiLA 2 server (with predefined IP / port)
  * successfully sending a SiLA command to the SiLA server from client with some parameters
  * SiLA 2 server using the sent parameter and generating a response, using this parameter
  * SiLA 2 client receiving this response and outputting the results
  * SiLA 2 client switching SiLA 2 server in simulation and real mode
  * showing different reactions / behaviour of the SiLA 2 server in simulation and real mode
  * illustrating basic error handling

For further steps, we highly encourage you to use the **sila2codegenerator**,
which is located in sila_tools/sila2codegenerator.
  * it converts your feature XML/FDL feature definitions into protobuf .proto files
  * it generates all gRPC interface stubs
  * it generates a server and test client
  * it generates simulation and real code templates, ready to run and ready to add your specific code.

You as developer just need to fill in the code for the application/device specific real hardware connections (serial/CAN/SPI/...) in the _real module.
