Contributing to sila\_python
============================

First off all, thank you for considering contributing to sila\_python.
It's people like **you** that could bring our vision alive that in the near
future most lab devices speak one common language **SiLA**....

This is a **more detailed** description of how you can contribute and how your code should look linke that some uniformity is maintained,
and readers do not get too much confused because of too different coding styles, but please keep in mind:

*"A Foolish Consistency is the Hobgoblin of Little Minds"*

We hope, that this reduces confusion of reading (and writing) code of (for) this repository.

**Thanks a gain for supporting us !**

How can you contribute ?
--------------------------

If you come across an *typo/issue/bug*, or have a *feature* request please open an
`Issue <https://gitlab.com/sila2/sila_python/issues>`_. If you want to
contribute (even fixing small typos are very welcome) or propose a new feature implementation please feel very welcome
by sending a `Merge Request <https://gitlab.com/sila2/sila_python/merge_requests>`_. **We need all hands and all your help !**

We also need volunteers writing/improving examples and the documentation, sample implementations
- or just testing/checking the code that it becomes rock-solid over the time.

Even if you are not a developer, but have some great ideas, please let us know
by submitting an issue through our `Issue Tracking System <https://gitlab.com/sila2/sila_python/issues>`_.

We are very happy for **all** your feedback ! **Thanks a lot !**

Filename Conventions
----------------------

Please follow the `PEP8 Style Guide <https://www.python.org/dev/peps/pep-0008/#id40>`_ recommendations:
  * **Python modules** should have *short, all-lowercase* names. *Underscores* can be used
    in the module name if it improves readability.
    Please choose names that tell more about what the module does, like: **serial_port_reader.py** instead of **reader.py**.
  * **Python packages** should also have *short, all-lowercase names*, although the use of *underscores is discouraged*.

Coding Style
-------------

For *consistancy and better readability* of the code and not confusing the readers with too many
different styles, we would like to ask you to follow a few rules when submitting code to the **SiLA_python** repository, which are briefly:

  * please use the recommendations of `PEP8 Style Guide <https://www.python.org/dev/peps/pep-0008>`_ for code formatting, but some differences are acceptable:
  * please use a maximum of 120 characters per line
  * to differntiate between a variable name and a mothod name,  camelCased module names are also acceptable (e.g. `myExcitingNewMethod()` )
  * please use tools like `Pylint <https://www.pylint.org/>`_ or Pylama to check your code readability
  * some function names might differ from the default, since all feature access functions are predefined in the SiLA standard and thus use CamelCase naming.
  * be careful with the new python 3 **format strings** ( `f"the content of variable my_var is:  {my_var}"` ):
    please only use them if the refering variable definition is visible within **approx. 20 lines** above.

Further reading:

  - `PEP8 at real python <https://realpython.com/python-pep8/>`_
  - `python guide <https://docs.python-guide.org/writing/style/>`_


Documentation
~~~~~~~~~~~~~

Please document your code __thoroughly__ ! Always keep in mind that there are many newcomers that want to understand, what you intented to say.
Be brief, but precise.
`Sphinx <https://www.sphinx-doc.org>`_ is used for documentation.
Sphinx uses **reStructured text** by default, therefore all documentation and READMEs should be
written in **reStructured text** inline documentation.

As an overview on how to write **reStructured text**,
the internet offers several `CheetSheets <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_.

.. code-block:: python

  def my_method(self, filename):
    """
    my_method makes life easier.

    :param filename: filename string
    """

File headers
~~~~~~~~~~~~~

Please use our Sphinx file *header template* with **Overview**, **Description** of the file, **Author** and **Creation Date**.


Quality Checking
-----------------

Please use tools, like `Pylint <https://www.pylint.org/>`_ or Pylama to check your code readability before submitting it.

Pylint
~~~~~~

This repository is already prepared to support Pylint with a given `.pylintrc <./.pylintrc>`_ configuration file.
To use this, make sure to run the ``pylint`` executable from the main project directory.

.. code-block:: console

    # check the sila2lib
    pylint --verbose ./sila_library/sila2lib

Using the ``--verbose`` flag will also output which ``.pylintrc`` file was loaded.

Submitting Issues or Asking for new features
----------------------------------------------

Please use our `Issue Tracking System <https://gitlab.com/sila2/sila_python/issues>`_  for submitting *issues* or requesting *new feature*.

Submitting your code
----------------------

If you want to contribute (even fixing small typos are very welcome) or propose a new feature implementation please feel very welcome
to send a `Merge Request <https://gitlab.com/sila2/sila_python/merge_requests>`_ .
This helps us to keep track of the branches.
Please mark code, which is *work in progress* with **WIP:** in the merge request title !!
This helps us to differentiate between a draft and final code.

Please use the following prefixes in your git branch title:

  **fix-** for fixing a bug

  **feature-** for adding a feature (not necessary a SiLA Feature)


git Submission
----------------

 * add short, but clear **commit statements**
 * when you generate a new branch and push it to gitlab, please use the --set-upstream (or -u) parameter the first
   time you push to GitLab, like `git push --set-upstream origin feature-my-new-feature`:
   this will ensure that when you push from this branch with a simple `git push` that the correct remote target branch will be updated.
 * we want an ultrafast user experience: newcomers should be ready to develop in less than 10min, therefore
   the repository shall be kept as small and lean as possible - have always a
   developer on a raspberry pi in mind with a slow internet connection: transmitting even 20 MB could be a pain.
 * avoid larger binaries, like .jpg/.png /.jar (>200kb) files they blow up
   the repository and slow down the download speed
 * please use our `git-workflow-avh <https://github.com/petervanderdoes/gitflow-avh>`_ as described in
   :doc:`10_git-workflow`.
 * add tags according to Versioning_


git Submission and Reviewing Process
-------------------------------------

We use the **git-workflow-avh** as described in `git-workflow-avh <https://github.com/petervanderdoes/gitflow-avh>`_
and in :doc:`10_git-workflow`.
All *Merge Requests* will be reviewed, tested and discussed by the active developers.
When the code is *tested*, *reliable* and *stable* it will be merged into master by the Repository Maintainer.
You are very welcome to comment on submitted code.

.. _versioning

Versioning
----------

The version in the sila\_python repository / sila\_library VERSION file
shall follow the `Semantic Versioning Pradigm <https://semver.org/>`_ prefixed by a 'v', briefly:

Given a version number vMAJOR.MINOR.PATCH, increment the:

  - MAJOR version when you make incompatible API changes,
  - MINOR version when you add functionality in a backwards compatible manner, and
  - PATCH version when you make backwards compatible bug fixes.

  Example: **v1.2.3**

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

Tags
~~~~

git Tags shall be assigned to every realease and shall follwo the Versioning_ scheme described above.


CI/CD pipline
~~~~~~~~~~~~~~

The `CI/CD pipeline <https://docs.gitlab.com/ee/ci/>`_ of gitlab is used to check the code for syntax flaws (**lints**) and errors (**unittests**) at every single submission.


Release
~~~~~~~

A new release is made, when all `milestones <https://gitlab.com/SiLA2/sila_python/-/milestones>`_ are reached. The numbering of the release follows the Versioning Scheme.


Acknowledgements
-----------------

We acknowledge and **thank** the following contributors to this repository:

.. include:: ../contributors.rst


.. include:: issues

.. _sila-python-gitlab-issues: https://gitlab.com/SiLA2/sila_python/issues
