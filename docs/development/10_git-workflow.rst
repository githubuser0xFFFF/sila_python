Git Workflow
=============

We use the **git-workflow** as described in
`git-workflow <https://nvie.com/posts/a-successful-git-branching-model/>`_ and

`git-workflow <https://www.atlassian.com/de/git/tutorials/comparing-workflows/gitflow-workflow>`_.

## Installation

On most recent **Linux** distributions, *git-flow* comes with the default package resources,
on Debian/Ubuntu systems you just call

.. code-block:: console

   sudo apt install git-flow

on **OSX** call

.. code-block:: console

   brew install git-flow

on **MSWindows** you can install: `git-flow for MSWindows <https://git-scm.com/download/win>`_

**!! please select a version with __support__ branches**

## git flow usage

There are basically two constant branches: *master* and *develop*.

*master* shall always contain a stable, tested release and the
*develop* branch is used for all progesses in development.
All *feature branches* are derived from *develop*.

When some features mature to be released, a *release branch* can be created.
In a *release branch*, no further features shall be added.
It is mainly used for bug-fixing and polishing.
Finalising a *release branch* will result in a merge to *master* and to *develop*.

The bugs in the *master branch* can be fixed with *hotfix branches*.

`Screencast git flow init <http://i.imgur.com/lFQbY5V.gif>`_

### Creating feature/release/hotfix/support branches

#### Feature branch

To start a new feature, use
.. code-block:: console

   git flow feature start [your_feature_branch]

To finish a feature:

.. code-block:: console

   git flow feature finish [your_feature_branch]

* Summary list/start/finish/delete feature branches, use:

.. code-block:: console

   git flow feature
   git flow feature start <name> [<base>]
   git flow feature finish <name>
   git flow feature delete <name>

  For feature branches, the `<base>` arg must be a branch, when omitted it defaults to the develop branch.

###  push/pull a feature branch to the remote repository, use:

.. code-block:: console

   git flow feature publish <name>
   git flow feature track <name>

###  list/start/finish/delete release branches, use:

.. code-block:: console

   git flow release
   git flow release start <release> [<base>]
   git flow release finish <release>
   git flow release delete <release>

For release branches, the `<base>` arg must be a branch, when omitted it defaults to the develop branch.

### list/start/finish/delete hotfix branches, use:

.. code-block:: console

   git flow hotfix
   git flow hotfix start <release> [<base>]
   git flow hotfix finish <release>
   git flow hotfix delete <release>

For hotfix branches, the `<base>` arg must be a branch, when omitted it defaults to the production branch.

###  list/start support branches, use:

.. code-block:: console

   git flow support
   git flow support start <release> <base>

For support branches, the `<base>` arg must be a branch, when omitted it defaults to the production branch.

### Share features with others

You can easily publish a feature you are working on. The reason can be to allow other programmers to work on it or to access it from another machine. The publish/track feature of gitflow simplify the creation of a remote branch and its tracking.

When you want to publish a feature just use:

.. code-block:: console

    git flow feature publish <name>

or, if you already are into the `feature/<name>` branch, just issue:

.. code-block:: console

   git flow feature publish


Now if you execute `git branch -avv` you will see that your branch `feature/<name>` tracks `[origin/feature/<name>]`. To track the same remote branch in another clone of the same repository use:

.. code-block:: console

   git flow feature track <name>

This will create a local feature `feature/<name>` that tracks the same remote branch as the original one, that is `origin/feature/<name>`.

When one developer (depending on your work flow) finishes working on the feature he or she can issue `git flow feature finish <name>` and this will automatically delete the remote branch. All other developers shall then run:

.. code-block:: console

   git flow feature delete <name>


to get rid of the local feature that tracks a remote branch that no more exist.

### Share hotfixes with others

You can publish an hotfix you are working on. The reason can be to allow other programmers to work on it or validate it or to access it from another machine.

When you want to publish an hotfix just use (as you did for features):

.. code-block:: console

   git flow hotfix publish <name>


or, if you already are into the `hotfix/<name>` branch, just issue:

.. code-block:: console

   git flow hotfix publish


Other developers can now update their repositories and checkout the hotfix:

.. code-block:: console

   git pull
   git checkout hotfix/<name>

and eventually finish it:

.. code-block:: console

   git flow hotfix finish
