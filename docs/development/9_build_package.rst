creating the package
====================

::

    cd [dir containing pack setup]
    python3 setup.py sdist

installing \_\_\_\_\_\_\_\_\_\_\_

::

    pip3 install --user pack

To uninstall the package, use pip3:

::

    pip3 uninstall pack

installing from setup
---------------------

This will install the package in the local python installation directory

::

     python3 setup.py install

creating the PyPi distribution
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

see https://packaging.python.org/tutorials/packaging-projects/

-  create virtual env (recommended)
-  install pypi tools

   pip install --upgrade pip pip install --upgrade setuptools wheel
   twine

-  Now run this command from the same directory where setup.py is
   located:

   python3 setup.py sdist bdist\_wheel

-  run Twine to upload all of the archives under dist:

   twine upload --repository-url https://test.pypi.org/legacy/ dist/\*

-  testing the installation (installing from
   https://test.pypi.org/simple/)

   pip3 install --index-url https://test.pypi.org/simple/ labpy

-  if tests are successful, install everything for real ....

   twine upload dist/\*
