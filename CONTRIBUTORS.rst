# Contributors to sila_python

* Maximilian Schulz
* Sebastian Hans
* Robert Giessmann
* Shaon
* Timm Severin (*timm.severin at tum dot de*)
* Florian Meinicke
* Stefan Born
* Thorsten
* Mark Doerr (*mark.doerr at uni-greifswald dot de*)
