sila\_python Implementations
============================

This is a collection of sample implementations. They can be used as
example implementations and inspirations. They are not necessarily fully
elaborated/developed products, suited for production environments.

Current categories:

-  analog\_digital\_converter
-  balances
-  barcode\_reader
-  camera\_video
-  centrifuges
-  container\_storage
-  dispensers
-  implementation\_concepts
-  incubators\_shakers
-  light\_detectors
-  liquid\_handlers
-  PCR
-  pumps
-  sila\_browser\_django
-  temperature\_controller
-  thermometers

New Ideas and Implementation Concepts
-------------------------------------

Proof-of-concept implementations and new ideas should be submitted in
the implemetation\_concepts folder.

Developers
----------

Please select one of the category folders, if your project fits and only
if there is nothing suited, create a new category folder, thanks. Refer
to the README file that each category contains, to see, if your project
fits.
