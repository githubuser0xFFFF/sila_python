SiLA2 Examples
==============

This folder contains examples featuring a certain aspect of
sila\_python. They written as tutorials for purely educational purposes
to illustrate how a certain feature works.

For the instructions on how to run the demos, please refer to the
**README.md** files within the demo directory

List of Examples
----------------

HelloSiLA2 Examples
~~~~~~~~~~~~~~~~~~~

HelloSiLA2\_minimal
^^^^^^^^^^^^^^^^^^^

This is a bare minimum example of what you need to establish a SiLA2
server-client communication.

HelloSiLA2\_full
^^^^^^^^^^^^^^^^

This is a more comprehensive HelloSiLA2 example, demonstrating also the
SiLAService functionality.

simulation\_real\_mode\_demo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This demo illustrates SiLA2 server simulation / real Mode switching

observable\_commands\_demo
~~~~~~~~~~~~~~~~~~~~~~~~~~

This demo illustrates how to work with observable commands

dynamic\_properties\_demo
~~~~~~~~~~~~~~~~~~~~~~~~~

This demo illustrates how to work with dynamic/observable properties

all\_datatypes\_demo
~~~~~~~~~~~~~~~~~~~~

This demo shows the usage of all basic datatypes

Hint
----

All examples were generated using the powerful **sila2codegenerator** in
python\_sila/sila\_tools/sila2codegenerator. Please have a look there
for more information.
