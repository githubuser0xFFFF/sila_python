#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelloSiLA2 client *

:details: HelloSiLA2 client: This is a test hello-SiLA2 service. 
           
:file:    HelloSiLA2_client.py
:authors: ben lear

:date: (creation)          2019-02-03T00:28:29.025844
:date: (last modification) 2019-02-03T00:28:29.025844

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

import logging
import argparse
import time

import grpc

import sila2lib.sila_client as s2client
from sila2lib.sila_service_detection import SiLA2ServiceDetection

from sila2lib import SiLAFramework_pb2 as fwpb2
from sila2lib.std_features import SiLAService_pb2 as spb2
from sila2lib.std_features import SiLAService_pb2_grpc as spb2g
from sila2lib.std_features import SimulationController_pb2 as scpb2
from sila2lib.std_features import SimulationController_pb2_grpc as scpb2g

import GreetingProvider_pb2
import GreetingProvider_pb2_grpc


class HelloSiLA2Client(s2client.SiLA2Client):
    """ Class doc """
    def __init__ (self, name="HelloSiLA2Client", service_name=None, sila_hostname="localhost", 
                  description="Description: This is a SiLA2 test client",
                  UUID = None, version="0.0", vendor_URL="lara.uni-greifswald.de",
                  ip='127.0.0.1', port=50001, key=None, cert=None):
        super().__init__(name=name,
                        service_name=service_name,
                        description=description,
                        UUID = UUID,
                        version=version,
                        sila_hostname=sila_hostname,
                        vendor_URL=vendor_URL,
                        ip=ip, port=port, 
                        key=key, cert=cert)
        
        """ Class initialiser 
            param cert=None: server certificate filename, e.g. 'sila_server.crt'  """
                
        self.GreetingProvider_serv_stub = GreetingProvider_pb2_grpc.GreetingProviderStub(self.channel)

    
    def run(self):
        try:
            print("SiLA client / client HelloSiLA2 -------- commands ------------")
            
            # --> put your remote calls here, e.g. (this is really only an example, please use the your corresponding calls)
            
            # calling SiLAService
            response = self.SiLAService_serv_stub.Get_ImplementedFeatures(spb2.ImplementedFeatures_Parameters() )
            for feature_id in response.ImplementedFeatures :
                logging.debug("implemented feature:{}".format(feature_id.FeatureIdentifier.value) )
            
            try:
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                   QualifiedFeatureIdentifier=spb2.DataType_FeatureIdentifier(FeatureIdentifier=fwpb2.String(value="SiLAService") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            try: 
                response = self.SiLAService_serv_stub.GetFeatureDefinition( 
                                spb2.GetFeatureDefinition_Parameters(
                                    QualifiedFeatureIdentifier=spb2.DataType_FeatureIdentifier(FeatureIdentifier=fwpb2.String(value="NoFeature") )) )
                logging.debug("get feat def-response:{}".format(response) )
            except grpc.RpcError as err:
                logging.debug("grpc/SiLA error: {}".format(err) )
            
            response = self.SiLAService_serv_stub.Get_ServerName(spb2.ServerName_Parameters() )
            logging.debug("display name:{}".format(response.ServerName.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerDescription(spb2.ServerDescription_Parameters())
            logging.debug("description:{}".format(response.ServerDescription.value) )
            
            response = self.SiLAService_serv_stub.Get_ServerVersion(spb2.ServerVersion_Parameters())
            logging.debug("version:{}".format(response.ServerVersion.value) )
            
            # testing commands --------------
            
            # --> calling GreetingProvider
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="DEFAULTstring sim1")))
                logging.debug("SayHello response:{}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )


            # testing properties ------------
            
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            # change to real mode    
            try :
                response = self.simulation_ctrl_stub.StartRealMode(scpb2.StartRealMode_Parameters())
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            # --> calling GreetingProvider
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="DEFAULTstring real" )))
                logging.debug("SayHello response:{}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )


            # testing properties ------------
            
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            # change back to simulation mode    
            try :
                response = self.simulation_ctrl_stub.StartSimulationMode(scpb2.StartSimulationMode_Parameters())
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )
                
            # --> calling GreetingProvider
            try :
                response = self.GreetingProvider_serv_stub.SayHello(GreetingProvider_pb2.SayHello_Parameters(Name=fwpb2.String(value="DEFAULTstring sim2" )))
                logging.debug("SayHello response:{}".format(response.Greeting.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )


            # testing properties ------------
            
            try :
                response = self.GreetingProvider_serv_stub.Get_StartYear(GreetingProvider_pb2.StartYear_Parameters())
                logging.debug("Get_StartYear response:{}".format(response.StartYear.value) )
            except grpc.RpcError as err:
                logging.error("grpc/SiLA error: {}".format(err) )

            
        except grpc._channel._Rendezvous as err :
            self.error_handler(err)

    def error_handler(self, errors):
        logging.error(errors._state)    

def parseCommandLine():
    """ just looking for command line arguments ...
       :param - : - """
    help = "SiLA2 service: HelloSiLA2"
    parser = argparse.ArgumentParser(description="A SiLA2 client: HelloSiLA2")
    parser.add_argument('-s','--server-name', action='store',
                         help='SiLA server to connect with [server-name]', default="HelloSiLA2")
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    
    return parser.parse_args()
    
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()

    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 client with service name: {servername}".format(servername=parsed_args.server_name))
        
        logging.info("starting SiLA2 client: HelloSiLA2")
        sila_client = HelloSiLA2Client(ip='127.0.0.1', port=55001 )
        sila_client.run()
