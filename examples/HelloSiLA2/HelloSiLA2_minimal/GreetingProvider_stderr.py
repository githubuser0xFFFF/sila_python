"""
________________________________________________________________________

:PROJECT: SiLA2_python

*greetingprovider_server_real *

:details: greetingprovider_server_real: Minimum Feature Definition as example. Provides a Greeting. 
           
:file:    greetingprovider_server_real.py
:authors: ben lear

:date: (creation)          2019-02-03T00:28:29.025844
:date: (last modification) 2019-02-03T00:28:29.025844

.. note:: Code generated automatically by SiLA2codegenerator v0.1.6!


           - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"




# Std Errors ... 
