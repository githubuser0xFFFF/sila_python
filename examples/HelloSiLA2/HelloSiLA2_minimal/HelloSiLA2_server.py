#!/usr/bin/env python3
"""
________________________________________________________________________

:PROJECT: SiLA2_python

*HelloSiLA2 *

:details: HelloSiLA2: This is a test hello-SiLA2 service. 
           
:file:    HelloSiLA2.py
:authors: ben lear

:date: (creation)          2019-02-03T00:28:29.025844
:date: (last modification) 2019-02-03T00:28:29.025844

.. note:: - 0.1.6
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""
__version__ = "0.1.1"

import logging
import argparse

import sila2lib.sila_server as slss

import GreetingProvider_pb2
import GreetingProvider_pb2_grpc

from GreetingProvider_servicer import GreetingProvider

class HelloSiLA2Server(slss.SiLA2Server):
    """ Class doc """
    def __init__ (self, parsed_args):
        super().__init__(name=parsed_args.server_name,
                         description=parsed_args.description,
                         server_type=parsed_args.server_type,
                         version=__version__, 
                         vendor_URL="hello06.uni-greifswald.de")
        
        """ Class initialiser """
        # registering features
        self.GreetingProvider_servicer = GreetingProvider()
        GreetingProvider_pb2_grpc.add_GreetingProviderServicer_to_server(self.GreetingProvider_servicer, self.grpc_server)
        self.addFeature('GreetingProvider', '.')
            
        # starting and running the gRPC/SiLA2 server
        self.run()
        
def parseCommandLine():
    """ just looking for commandline arguments ...
       :param - : -"""
    help = "SiLA2 service: HelloSiLA2"
    
    parser = argparse.ArgumentParser(description="A SiLA2 service: HelloSiLA2")
    parser.add_argument('-s','--server-name', action='store',
                         default="HelloSiLA2", help='start SiLA server with [server-name]' )
    parser.add_argument('-t','--server-type', action='store',
                         default="TestServer", help='start SiLA server with [server-type]' )
    parser.add_argument('-d','--description', action='store',
                         default="This is a test hello-SiLA2 service", help='SiLA server description' )
    parser.add_argument('-v','--version', action='version', version='%(prog)s ' + __version__)
    return parser.parse_args()
    
    
if __name__ == '__main__':
    """Main: """
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    
    parsed_args = parseCommandLine()
                
    if parsed_args.server_name :
        # mv to class
        logging.info("starting SiLA2 server with server name: {server_name}".format(server_name=parsed_args.server_name))
        
        # generate SiLAserver with class defined above
        sila_server = HelloSiLA2Server(parsed_args=parsed_args )
        
