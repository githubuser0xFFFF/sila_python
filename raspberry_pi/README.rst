SiLA\_python on the Raspberry Pi
================================

Here you shall find all Raspberry Pi specific information to quickly
setup your Raspberry Pi to run SiLA on it.

This can e.g. be the basis for converting a non-SiLA device into a real
SiLA device.

Installation
------------

Installer Script
~~~~~~~~~~~~~~~~

Manual Installation
~~~~~~~~~~~~~~~~~~~

Docker
~~~~~~

Hardware
--------

Serial Port Connection
~~~~~~~~~~~~~~~~~~~~~~

GPIO
~~~~

CAN
~~~

I2C
~~~

SPI
~~~

Camera
~~~~~~
