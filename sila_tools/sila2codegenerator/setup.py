"""_____________________________________________________________________

:PROJECT: SiLA2_python

*SiLA2 python3 codegenerator setup*

:details: SiLA2 python3 codegenerator setup.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20180610
:date: (last_modified)     2019-08-21
________________________________________________________________________
"""

__version__ = "0.2.0"

import os

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

#~ LIB_SRC_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
#~ pkg_dir = os.path.join(LIB_SRC_DIRECTORY, 'src')

pkg_name = 'sila2codegen'

def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file :
        return file.read()

install_requires = ["lxml"]

setup(  name=pkg_name,
        version=__version__,
        description='SiLA2 code generator for Python3',
        long_description=read('README.rst'),
        author='Mark Doerr, Timm Severin, Florian Meinicke',
        author_email='mark.doerr@uni-greifswald.de',
        keywords=('SiLA2, codegenerator, lab automation,  laboratory, instruments,'
                  'experiments, evaluation, visualisation, serial interface, robots'),
        url='https://gitlab.com/SiLA2/sila_python',
        license='MIT',
        packages=find_packages(),
        #~ package_dir={'': pkg_dir},
        #~ package_dir={'codegenerator':'codegenerator'},
        install_requires = install_requires,
        test_suite='',
        classifiers=[  'License :: OSI Approved :: MIT',
                       'Intended Audience :: Developers',
                       'Operating System :: OS Independent',
                       'Programming Language :: Python',
                       'Programming Language :: Python :: 3.6',
                       'Programming Language :: Python :: 3.7',
                       'Topic :: Utilities',
                       'Topic :: Scientific/Engineering',
                       'Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator',
                       'Topic :: Scientific/Engineering :: Information Analysis'],
        include_package_data=True,
        package_data={pkg_name: ['templates/*.pyt', ]},
        entry_points={
        'console_scripts': [
            'sila2codegenerator=sila2codegen.sila2codegenerator:main'
        ]
    },
        setup_requires=['wheel']
        #~ data_files=data_files,
        #~ zip_safe=False
)
